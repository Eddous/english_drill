import os
from collections import deque
import random
import sys

class PriorityList():
    def __init__(self):
        self.learned = []
        self.sum = 0
        self.not_learned = []

    def add(self, word):
        if word.is_learned():
            self.learned.append(word)
            self.sum += word.get_priority()
        else:
            self.not_learned.append(word)

    def get(self):
        if self.not_learned_chance() > random.random():
            index = random.randrange(len(self.not_learned))
            return self.not_learned.pop(index)

        number = random.uniform(0, self.sum)
        for word in self.learned:
            word_priority = word.get_priority()
            number -= word_priority
            if number <= 0:
                self.learned.remove(word)
                self.sum -= word_priority
                return word
        exit("panic")
    
    def not_learned_chance(self):
        l = len(self.not_learned) 
        if l < 10:
            return 0.5
        if l > 50:
            return 0.9
        return 0.01*l + 0.4

class Word:
    def __init__(self, line):
        splited = line.split(";")
        if len(splited) == 2: # not inited yet
            self.cz_suc = 0
            self.en = splited[0].strip()
            self.cz = splited[1].strip()
            self.en_suc = 0
            return
        
        if len(splited) == 3:
            self.cz_suc = 0
            self.en = splited[0].strip()
            self.cz = splited[1].strip()
            self.en_suc = 1
            return

        if len(splited) == 4:
            self.cz_suc = int(splited[0].strip())
            self.en = splited[1].strip()
            self.cz = splited[2].strip()
            self.en_suc = int(splited[3].strip())
            return
        exit("panic")
    
    def str_to_file(self):
        return str(self.cz_suc) + " ; " + self.en + " ; " + self.cz + " ; " + str(self.en_suc)

    def str_to_stdout(self):
        if MODE:
            print("(" + str(self.en_suc) + ") " +  self.cz, end=" - ")
        else:
            print("(" + str(self.cz_suc) + ") " +  self.en, end="")

    def get_priority(self):
        if MODE:
            return priority_trasformator(self.en_suc)
        return priority_trasformator(self.cz_suc)
    
    def is_learned(self):
        if MODE:
            return self.en_suc != 0
        return self.cz_suc != 0


def priority_trasformator(num):
    return 1/2**num

def read_database():
    database = open(FILE, "r")
    lines = database.readlines()
    database.close()

    word_database = []
    words = PriorityList()
    for i, line in enumerate(lines):
        print(i+1)
        if len(line.strip()) == 0:
            continue
        word = Word(line)
        word_database.append(word)
        words.add(word)
        
    return word_database, words


def write_database(words):
    with open(FILE + ".tmp", "w+") as database:
        for word in words:
            database.write(word.str_to_file() + "\n")
    os.rename(FILE + ".tmp", FILE)


def load(queue, words):
    while len(queue) != PRE_LOAD:
        queue.append(words.get())


def num_not_learned(lis):
    i = 0
    for word in lis:
        if not word.is_learned():
            i += 1
    return i

FILE = sys.argv[1]
if sys.argv[2] != "0" and sys.argv[2] != "1":
    exit("wrong mode")
MODE = sys.argv[2] == "2"
PRE_LOAD = 10

database, words = read_database()
queue = deque()

while True:
    print()
    load(queue, words)
    print(num_not_learned(queue) + len(words.not_learned))

    word = queue.popleft()
    word.str_to_stdout()
    if MODE:
        if input() == word.en:
            print("CORRECT")
            word.en_suc += 1
        else:
            print("correct was:", word.en)
            word.en_suc = 0
    else:
        input()
        print(word.cz, "- ", end="")
        if input() == "y":
            word.cz_suc += 1
        else:
            word.cz_suc = 0
    
    words.add(word)
    write_database(database)

